#!/usr/bin/env python
# -*- encoding: UTF8 -*-
# Author: Philipp Klaus, philipp.l.klaus AT web.de


#   This file is part of netio230a.
#
#   netio230a is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   netio230a is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with netio230a.  If not, see <http://www.gnu.org/licenses/>.


## import the netio230a class:
import netio230a
## for sys.exit(1)
import sys
import optparse
import configuration

def GetSocket(switchhost,sockets):
    for i in range(0,4):
        if (sockets[i].getName() == switchhost):
            return i+1
    print ("no Socket with this name")
    return -1

def main(argv=None):
    if argv is None:
        argv = sys.argv
    # parse command line options
    p = optparse.OptionParser()
    p.add_option('--hostname', '-n',help="hostname. Host which should be switched")
    p.add_option('--device', '-d',help="hostname of powerswitch-device")
    p.add_option('--state', '-s',type="choice",choices=["on","off"],help="state. 'on' or 'off' ",default="on")
    p.add_option('--verbose', '-v',help="verbosity",action="store_true")
    p.add_option('--checkHost', '-c', help="Check, which host is online",default="no")
    options, arguments = p.parse_args()
    switchhost = options.hostname
    # If loop to only check the status of certain hosts, not to switch off or on
    if (options.checkHost== "yes") :
        devices= configuration.getConfiguration()
        device=devices[0]
        try:
            netio = netio230a.netio230a(device[1], device[3], device[4], True, device[2])
        except StandardError:
            print("Could not connect")
        power_sockets = netio.getAllPowerSockets()
        power_socket_to_change = GetSocket(switchhost,power_sockets)
        isOn= power_sockets[power_socket_to_change-1].getPowerOn()
        #print ("%s %s \n" % (isOn,power_sockets[power_socket_to_change-1].getName()))
        if (isOn == 1):
            sys.exit(0);
        else :
            sys.exit(1);
    power_on = options.state
    if options.state == "on" : 
        power_on = True
    else:
        power_on = False
    #get credentials from config
    devices = configuration.getConfiguration()
    #TODO: only one device for now
    #for device in devices:
    #device name, IP, port, user, password
    device = devices[0]
    #print device[1],str(device[2]),device[3],device[4]
    #TODO: manual saving of configuration for now
    #configuration.changeConfiguration(configuration.UPDATE, options.device, options.device, 1234, "admin", "nam8272")
    try:
        netio = netio230a.netio230a(device[1], device[3], device[4], True, device[2])
    except StandardError:
        print("could not connect")

    power_sockets = netio.getAllPowerSockets()
    #get socket from name
    power_socket_to_change = GetSocket(switchhost,power_sockets)
    if power_socket_to_change == -1: return 1
    #change socket-status
    netio.setPowerSocketPower(power_socket_to_change,power_on)
    power_sockets = netio.getAllPowerSockets()
    
    netio = None

    if options.verbose:
        # print response
        print ("\n--------- successfully queried the Koukaam NETIO 230A ---------")
        print ("-- set power socket %s (%s) to: \"%s\"" % (power_socket_to_change,switchhost,power_on) )
        print ("-- power status --\n")
        for i in range(0,4) :
            print ("power socket %d (%s): %s" % (i,power_sockets[i].getName(), power_sockets[i].getPowerOn()) ) 

        print "---------------------------------------------------------------- \n"
    

if __name__ == '__main__':
    sys.exit(main())


