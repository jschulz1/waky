#!/usr/bin/perl

use strict;
use warnings;

#our config dir
use constant CONFIG_DIR => '/etc';

#configfile
use constant CONFIG_FILE => 'waky.conf';

#for parsing our config file
use Config::IniFiles;

#the config-file hash
my %ini;
tie %ini, 'Config::IniFiles', ( -file => CONFIG_DIR . "/" . CONFIG_FILE );

#for Database access
use DBI;

use Net::LDAP;
use Net::LDAP::Entry;
use Net::LDAP::LDIF;
use Net::LDAP::Util qw(ldap_error_desc);

#check time differences
use DateTime::Format::MySQL;

#for reverse lookup, we need the ip from the hostname
use Socket;

#to output or data into a mediawiki
use MediaWiki::API;

#for the json backend
use JSON;
use LWP::UserAgent;

#write a pid file
use Proc::PID::File;

#start daemonized
use Proc::Daemon;

#for our logfiles
use Log::Dispatch;
use Log::Dispatch::File;
use Date::Format;
use File::Spec;

#for command line options
use Getopt::Long;
use Digest::MD5;

#the name of this executable
our $ME = $0;
$ME =~ s|.*/||;

#dir we put the pid file in
my $PIDDIR = $ini{'options'}{'piddir'};

#complete path of the pidfile
our $PIDFILE = $PIDDIR . "/$ME.pid";

#get and process our hostname
our $HOSTNAME = `hostname`;
chomp $HOSTNAME;

#dir we put our logfiles in
my $LOGDIR = $ini{'options'}{'logdir'};

#our main log files name
my $LOGFILE = $ini{'options'}{'logfile'};

#if the load of a computer is below this limit we shut try to him down
my $SHUTDOWN_LIMIT = $ini{'options'}{'shutdownlimit'};

#if the entire load is too high we need to wakeup another computer
my $WAKEUP_LIMIT = $ini{'options'}{'wakeuplimit'};

#fork and detatch from the controlling console
my $daemon = $ini{'options'}{'daemon'};

#be verbose
my $verbose = $ini{'options'}{'verbose'};

#reopen stdin,stdout and stderr in daemon mode and write /tmp/waky-error
my $debug = $ini{'options'}{'debug'};

#time we wait after one cycle
my $waittime = $ini{'options'}{'waittime'};

#dont edit the bind config
my $dryrun = $ini{'options'}{'dry'};
my $bindconf = $ini{'options'}{'rewritebind'};

my $config_backend = $ini{'options'}{'config-backend'};

my $data_backend = $ini{'options'}{'data-backend'};

my $htmlUpdateFlag = $ini{'options'}{'html'};

#if false, only check the status of the computers, dont decide whether or not to switch them off
my $monitorFlag = $ini{'options'}{'monitorflag'};

my $snmpPass = $ini{'SNMP'}{'pass'};

my $snmpUser = $ini{'SNMP'}{'user'};

# host data structure:
# %host is a hash of hashes. you can get a dataset of a host by using its name as
# key.
# %host = $hosts->{'hostname'};
# The data of one host is mostly collected by a mysql query to the ndo-database of nagios.
# Future backends will follow.
#
# A host itself is a hash containing the following keys:
# $host->{'mac'}: macadress of the host
# $host->{'bootable'}: flag that sets the possibility of WOL of this host
# $host->{'memory'}: the percentage of used memory
# $host->{'load'}: the load of this host
# $host->{'users'}: the current number of users logged in on this host
# $host->{'ip'}: the ip of the host
# $host->{'last_status_change'}:
# $host->{'powerdownflag'}: defines if a computer can be shut down. 1 means, computer may be shut down.
# $host->{'cf'}: cost functional of this host, calculation based on load, mem use and users on the host

my %groups = ();

# disable output buffering
$| = 1;

GetOptions(
	'verbose'   => \$verbose,
	'debug'     => \$debug,
	'daemonize' => \$daemon,
	'waittime'  => \$waittime,
	'dryrun'    => \$dryrun
);

#
# Setup a logging agent
#
my $log = new Log::Dispatch(
	callbacks => sub {
		my %h = @_;
		return Date::Format::time2str( '%B %e %T', time ) . " " . $HOSTNAME . " $0\[$$]: " . $h{message} . "\n";
	}
);
$log->add(
	Log::Dispatch::File->new(
		name      => 'file1',
		min_level => 'warning',
		mode      => 'append',
		filename  => File::Spec->catfile( $LOGDIR, $LOGFILE ),
	)
);

#check if we start as a daemon or inside a terminal
if ($daemon)
{
	if ($debug){
	  output("Starting daemonized!\n");
	}
	&start_Daemonized();
}
else
{	
	if ($debug){
	  output("Starting waky in console\n");
	}
	&main();
}

sub dienice ($);

#print out to a logfile or to STDOUT
sub output
{
	my $outputString = shift;
	if ( $daemon == 1 )
	{
		$log->warning($outputString);
	}
	else
	{
		print("$outputString");
	}
}

#
#start as a daemon
#
sub start_Daemonized
{

	#
	# fork and background process
	#
	if ($debug){
	  output("Initialization of the Daemon\n");
	}
	Proc::Daemon::Init();
	if ($@)
	{
		dienice("Unable to start daemon:  $@");
	}

	#
	# Get a PID file
	#
	if ($debug){
	  output("Getting a PID file\n");
	}

	die "Already running!" if Proc::PID::File->running();

	output( "Waky is starting  " . time() . "\n" );

	if ($debug)
	{
		close(STDIN);
		close(STDOUT);
		close(STDERR);
		open( STDIN,  "+>/tmp/waky-error" );
		open( STDOUT, "+>&STDIN" );
		open( STDERR, "+>&STDIN" );
	}
	&main;

}

#
#the main routine
#
sub main
{
	my $keep_going = 1;

	my %hosts = ();

	#
	# Setup signal handlers so that we have time to cleanup before shutting down
	#
	$SIG{HUP}  = sub { output("Caught SIGHUP:  exiting gracefully\n");  $keep_going = 0; };
	$SIG{INT}  = sub { output("Caught SIGINT:  exiting gracefully\n");  $keep_going = 0; };
	$SIG{QUIT} = sub { output("Caught SIGQUIT:  exiting gracefully\n"); $keep_going = 0; };
	$SIG{TERM} = sub { output("Caught SIGTERM:  exiting gracefully\n"); $keep_going = 0; };
	
	
	#
	# enter main loop
	#
	while ($keep_going)
	{
		#read the config backend and fill the hosts hash
		if($debug){
		  output("Reading config\n");
		}
		
		&readConfig( \%hosts, \%groups, $config_backend );
		
		
		#oO: generate the correct host hashes for all the groups

		# number of Offline Compters
		my $offlineCount=0;
		output("Pinging all computers to check which are online... \n");

		#ping all hosts and see if they respond, if not,  increase offlineCount
		foreach my $host ( keys(%hosts) )
		{
		      
			# test here: uninitialized value!
			if ( ( executeCommand("ping -W 1 -c 1 -q $host") )[1] eq 1 )
			{
                #host is offline
				
				$offlineCount +=1;
                                if($debug){
                                    output("Host $host is offline.\n");
                                }
				$hosts{$host}{'online'} = 0;

				#x2go looks in ldap, if a host is reachable
				my $domain = $hosts{$host}{'domain'};
				if ($ini{'options'}{'x2go'} == 1) { setX2GOSerialNumber( $host,$domain, 0 ); }
				if($debug){
                                    output("Checking whether or not $host is connected to an ip-switchable power-device...\n");
                                }
                
				#switch of powersupply of computer if connected to ip-switchable powersupply
				if ( $hosts{$host}{'boottype'} eq "power" )
				{	
                                        if($debug){
                                           output("Host is connected to ip-switchable power-device...\n");
                                        }
					# check whether or not if switching off is necessary
                                        if($debug){
                                           output("Checking whether or not the power supply is already switched off...\n");
                                        }
					my $switchOffNecessary = checkNecessityOfSwitchOff($host);
					
					if ($switchOffNecessary == 0){
						if ($verbose || $debug){
							output("Switchoff for host $host necessary.\n");
						}
						switchOffPowerSupply($hosts{$host}{'bootoptions'},$host);
					}
					else {
						if ($verbose ||$debug){
							output("Power supply of host $host already switched off.\n");
						}
					}
					
				}
				
			}
			else
			{
                                if($debug){
                                    output("Host $host is online.\n");
                                }
				$hosts{$host}{'online'} = 1;
				my $domain = $hosts{$host}{'domain'};
				#x2go looks in ldap, if a host is reachable
				if ($ini{'options'}{'x2go'} == 1) { setX2GOSerialNumber( $host,$domain, 1 ); }
			}
			
		}
		output("Getting data from backend '$data_backend'\n");
				
		print( "Hosts up:   ", scalar( keys %hosts ) - $offlineCount, "\n" );
		print( "Hosts down: " . $offlineCount . "\n" );

		#Get information about the hosts via configured data backend.
		#also, process them (calculate the costFunctional)
		&getHostData( \%hosts );

		#write the data we got via configured output-plugin
		#&editMediaWiki(\%hosts);
		if ($htmlUpdateFlag){
			output("Updating HTML now!\n");
			&outputHTML(\%hosts);
		}
		else {
			output("Skipped updating HTML because flag is off.\n");
		}


		#decide wether to boot or shutdown a computer
		if ($monitorFlag ==0){
			if ($debug){
				output("Monitoring only is switchted off\n");	
			}
			&decideAndExecute( \%hosts );
		}
		else {
			output("Monitoring run finished!\n");
		}
		output("#### Run finished  - sleeping ####\n");
		if ($debug){
			output("Time until next run is $waittime.\n");
		}
		sleep $waittime;
	}

	#
	# Mark a clean exit in the log
	#
	output( "Waky is stopping  " . time() . "\n" );

	# Can't locate object method "release" via package "Proc::PID::File" at ./waky.pl line 301, <DATA> line 466.
	# Change: only call release when starting daemonized; else there was no init() and release() is impossible
	if ($daemon){
		Proc::PID::File->release();
	}
}

#
# getHostData from backend specified in config
#
sub getHostData
{
	my $hosts = shift;
	if ( $data_backend eq "data-ndo" )
	{
		if ($debug){
		output("Data backend is data-ndo!\n");
		}
		&getDataFromNagios($hosts);
	}
	elsif ( $data_backend eq "data-json" )
	{
		if($debug){
		output("Data backend is data-json!\n");
		}
		&getDataFromJsonQuery($hosts);
	}
	else
	{
		die "No valid data backend specified.";
	}
}

#
# dienice
#
# write die messages to the log before die'ing
#
sub dienice ($)
{
	my $output = shift;
	output( "Received" . $output . "Shutting down\n" );
}

#
#preprocessing of the data we get from nagios (perfdata), arguments are the name of the host and a adress to the
#hash containing all hosts data
#
sub parseHosts
{
	if ($debug){
		output("Preprocessing of the data got from nagios, arguments are the name of host and an adress to hash containing host data.\n");
	}
	my $host  = shift;
	my $hosts = shift;

	#parse load
	if ( $hosts->{$host}{'load'} )
	{
		$hosts->{$host}{'load'} =~ m/.*load15=(\d?\d\.\d\d).*/;
		if ($debug){
		output("Load of $host is $1");
		}
		$hosts->{$host}{'load'} = $1;
	}
	else
	{
		if($debug){
		output("Could not find load of the host. Setting it to 0 by default\n");
		}
		$hosts->{$host}{'load'} = 0;
	}

	#parse memory/
	if ( defined( $hosts->{$host}{'memory'} ) )
	{
		$hosts->{$host}{'memory'} =~ m/used=([0-9]*)o size=([0-9]*)o/;
		if ( defined($1) && defined($2) && $2 != 0 )
		{
			my $ratio = $1/$2;
			if ($debug){
			output("Memory of $host is $ratio.\n");
			}
			$hosts->{$host}{'memory'} = $ratio;
		}
	}
	else
	{
		if($debug){
		output("No memory was defined for $host. Setting memory usage default to 0.1 .\n");
		}
		$hosts->{$host}{'memory'} = 0.1;
	}

	#parse users
	if ( $hosts->{$host}{'users'} )
	{
		
		$hosts->{$host}{'users'} =~ m/users=(.*)u/;
		if ($debug){
		output("There are $1 users on $host.\n");
		}
		$hosts->{$host}{'users'} = $1;
	}
	else {
		$hosts->{$host}{'users'} = 0;
	}

	#get ip
	$hosts->{$host}{'ip'} = inet_ntoa( inet_aton($host) );

	if ( exists( $hosts->{$host}{'last_status_change'} ) )
	{
		
		my $dthost     = DateTime::Format::MySQL->parse_datetime( $hosts->{$host}{'last_status_change'} );
		my $dtlocal    = DateTime->now( time_zone => 'Europe/Berlin' );
		# time after booting when a shutdown is forbidden; exclude in a config data possibly
		my $pdinterval = DateTime::Duration->new( hours => 2 );
		
		if ( $dtlocal - $pdinterval < $dthost )
		{
			if ($debug){
			output("Setting powerdownflag for $host to zero, because $dtlocal - $pdinterval < $dthost. Minimum online time was not reached.\n");
			}
			$hosts->{$host}{'powerdownflag'} = 0;
		}
		else
		{
			if ($debug){
			output("Setting powerdownflag for $host to true, because $dtlocal - $pdinterval > $dthost. Minimum online time was reached.\n");
			}
			$hosts->{$host}{'powerdownflag'} = 1;
		}
	}
}

#
# json backend helper: http request
#
sub fetchPageJsonBackend
{
	my $url = shift;
	my $ua  = LWP::UserAgent->new();
	system("kinit -k");
	my $response = $ua->get($url);
	if ( $response->is_success )
	{
		return $response->content;
	}
	else
	{
		die "LWP failed: " . $response->status_line . "\n";
	}
}

#
# json backend helper: host attribute extractor, parses as well
#
sub getHostAttributesJsonBackend
{
	
	my $hstruct = shift;
	my $host    = shift;
	if ($debug){
	  output("Extracting host attributes by parsing Json data for host $host...\n");
	}

	my %hostattr;

	for (@$hstruct)
	{
	  #can be used to check the regex or the keys of the parsed icinga/json file
	  #because note and action of url are uninitialized -> set to none
		$_{'action_url'} = "none";
		$_{'notes_url'} = "none";
		# use of hash reference is deprecated
		#%$_->{'action_url'} = "none";
		#%$_->{'notes_url'} = "none";
		if ($debug){
	    		foreach my $key (keys(%$_)){
		  		output($key . "\n");
		  		my $hash = $_;
		  		output($hash->{"$key"} . "\n");
		  
	    		}
	  	}
		if (exists ($_->{'host_name'})){
			if ($debug){
			  output("Icinga version above 1.8.0 detected.\n");
			}
		  	# keys only functional for icinga cgi_json_version: 1.8.0
		  	next unless ( $_->{'host_name'} =~ /$host\..+?/ );    # no eq to match "foo.bar" just as "foo"
		  	if ( $_->{'service_description'} eq 'users' ) {
				$_->{'status_information'} =~ /users logged in (\d+)/;
				if ($debug){
				  output("$1 users logged in on $host.\n");
				}
				$hostattr{'users'} = $1;
				$_->{'duration'} =~ /(\d+)d\s+(\d+)h\s/;
				if ( $1 == 0 and $2 < 2 )
				{
					$hostattr{'powerdownflag'} = 0;
				}
				else
				{
					$hostattr{'powerdownflag'} = 1;
				}
				$hostattr{'last_status_change'} = $_->{'duration'};
		  	}
		  	if ( $_->{'service_description'} eq 'load' )
		  	{
				$_->{'status_information'} =~ /load average: (\d?\.\d\d+?),/;
				if ($debug){
				  output("Load of $host is $1.\n");
				}
				$hostattr{'load'} = $1;
		  	}
		  	if ( $_->{'service_description'} eq 'memory' )
		  	{
				$_->{'status_information'} =~ /Ram : (\d+?)%/;
				if ($debug){
				  output("Memory usage of $host is 0. $1.\n");
				}
				$hostattr{'memory'} = "0." . $1;
			}
		}
		elsif (exists ($_->{'host'})){
			#in case the icinga version is below 1.8.0
		  	next unless ( $_->{'host'} =~ /$host\..+?/ );    # no eq to match "foo.bar" just as "foo"
		  	if ( $_->{'service'} eq 'users' )
		  	{
				$_->{'status_information'} =~ /users logged in (\d+)/;
				if ($debug){
				  output("$1 users logged in on $host.\n");
				}
				$hostattr{'users'} = $1;
		  	}
		  	if ( $_->{'service'} eq 'load' )
		  	{
				$_->{'status_information'} =~ /load average: (\d?\.\d\d+?),/;
				if ($debug){
				  output("Load of $host is $1.\n");
				}
				$hostattr{'load'} = $1;
		  	}
		  	if ( $_->{'service'} eq 'memory' )
		  	{
				$_->{'status_information'} =~ /Ram : (\d+?)%/;
				if ($debug){
				  output("Memory usage of $host is 0. $1.\n");
				}
				$hostattr{'memory'} = "0." . $1;
			  }
			if ( $_->{'service'} eq 'users' )
			{
				$_->{'duration'} =~ /(\d+)d\s+(\d+)h\s/;
				if ( $1 == 0 and $2 < 2 )
				{
					$hostattr{'powerdownflag'} = 0;
				}
				else
				{
					$hostattr{'powerdownflag'} = 1;
				}
				$hostattr{'last_status_change'} = $_->{'duration'};
			}
		}
		else {
			die("Check icinga version; No parsing of mandatory attributes possible \n");
		}
	}
	return \%hostattr;
}

#
# json backend: main query routine
#
sub getDataFromJsonQuery
{
	if ($debug){
	  output("Fetching Data from Json query...\n");
	}
	my $hosts = shift;
	my $url   = $ini{'data-json'}{'url'};
	if ($debug){
	  output("URL is $url\n");
	}
	# fetch json from http
	my $page = '';
	$page = fetchPageJsonBackend($url);
	if ($debug) {
	  output( "Json output: \n");
	  output($page . "\n");
	}
	

	# decode json and extract host struct
	my $json    = JSON->new();
	my $dstruct = $json->utf8->decode($page);
	my $hstruct = $dstruct->{'status'}->{'service_status'};

	# handle all hosts online
	for ( keys(%$hosts) )
	{
		my $host = $_;

		next unless ( $hosts->{$host}{'online'} );

		if ($verbose)
		{
			print("================================\n\t$host\n================================\n");
		}

		my $hostattr = getHostAttributesJsonBackend( $hstruct, $host );
		for ( keys(%$hostattr) )
		{
			$hosts->{$host}{$_} = $hostattr->{$_};
		}
		&costFunctional( $host, $hosts );
		if ( $verbose == 1 )
		{
			foreach my $datum ( keys( %{ $hosts->{$host} } ) )
			{
				if ( defined( $hosts->{$host}->{$datum} ) )
				{
					output( "$datum: " . $hosts->{$host}->{$datum} . "\n" );
				}
			}
			output("\n");
		}
	}
}

#
# connects to the ngo Database from nagios
#
sub getDataFromNagios
{
	my $hosts    = shift;
	if ($debug){
	  output("Getting data from nagios...\n");
	}
	my $hostname = $ini{'data-ndo'}{'host'};
	my $database = $ini{'data-ndo'}{'database'};
	my $user     = $ini{'data-ndo'}{'user'};
	my $pass     = $ini{'data-ndo'}{'pass'};
	if ($debug){
	  output("Hostname is $hostname\n");
	  output("Database is $database\n");
	  output("User is $user\n");
	  output("Password is $pass\n");
	}
	my $dsn      = "DBI:mysql:database=$database;host=$hostname";
	if ($debug){
	  output("DSN is $dsn\n");
	}
	my $dbh      = DBI::->connect( $dsn, $user, $pass, { 'RaiseError' => 1, 'AutoCommit' => 1 } )
	  or die DBI::errstr;

	foreach my $host ( keys %{$hosts} )
	{
		if ( $hosts->{$host}{'online'} )
		{
			my $sth = $dbh->prepare(
"select nagios_services.display_name, nagios_servicestatus.output, nagios_servicestatus.perfdata, nagios_hoststatus.last_state_change from nagios_servicestatus INNER JOIN nagios_services ON  nagios_services.service_object_id=nagios_servicestatus.service_object_id AND config_type=1 LEFT JOIN nagios_hosts ON nagios_services.host_object_id=nagios_hosts.host_object_id LEFT JOIN nagios_hoststatus ON nagios_services.host_object_id=nagios_hoststatus.host_object_id WHERE nagios_hosts.display_name='$host' AND nagios_hosts.config_type=1 AND (nagios_services.display_name='load' OR nagios_services.display_name='memory' OR nagios_services.display_name='users')"
			) or die "Couldn't prepare statement: " . $dbh->errstr;
			$sth->execute();
			my @data = ();
			if ($verbose)
			{
				print("================================\n\t$host\n================================\n");
			}
			while ( @data = $sth->fetchrow_array() )
			{
				$hosts->{$host}{ $data[0] } = $data[2];
				$hosts->{$host}{'last_status_change'} = $data[3];

				# calculate cost-functional
			}
			&parseHosts( $host, $hosts );
			&costFunctional( $host, $hosts );
			if ( $verbose == 1 )
			{
				foreach my $datum ( keys( %{ $hosts->{$host} } ) )
				{
					if ( defined( $hosts->{$host}->{$datum} ) )
					{
						output( "$datum: " . $hosts->{$host}->{$datum} . "\n" );
					}
				}
				output("\n");
			}
		}
	}
	$dbh->disconnect;
}

sub outputHTML
{	
	if ($debug){
	  output("Writing html...\n");
	}
	my $hosts        = shift;
	my %printoptions = ( 'memory' => 1, 'users' => 1, 'load' => 1 );
	my $wakecgi = $ini{'html'}{'wakecgi'};
	my $inputstring = qq(<table border=2 frame=void>\n  <tr>\n    <th>Online computer</th>\n);
	my @hostlist    = keys( %{$hosts} );
	foreach my $service ( keys %printoptions )
	{
	   $inputstring .= "    <th> $service </th>\n";
	}
	$inputstring .= "  </tr>\n";

	foreach my $host ( keys %{$hosts} )
	{
		if ( $hosts->{$host}{'online'} )
		{
			$inputstring .= "  <tr>\n    <td>$host</td>\n";
		  foreach my $service ( keys %printoptions )
		  {  
				#if($hosts->{$host}{$service})
				#{
				if ( $service eq 'memory' )
				{
					my $roundedmem = sprintf( "%.2f", $hosts->{$host}{$service} ) * 100;
					$inputstring .= "    <td>$roundedmem%</td>\n";
				}
				else
				{
					$inputstring .= "    <td>$hosts->{$host}{$service}</td>\n";
				}

				#}
				#else
				#{
				#        $inputstring .= "|| ";
				#}
		  }
		  $inputstring .= "  </tr>\n";
		}
	}

	$inputstring .=
qq(</table>\n<br>\n<br>\n<table border=2 frame=void>\n  <tr>\n    <th>Offline computer</th>\n    <th>Boot</th>\n  </tr>);
	# to replace @macsdown
	foreach my $host (keys %$hosts){
	    if (!$hosts->{$host}{'online'} && $hosts->{$host}{'bootable'} == 1){
		my $macAddress = $hosts->{$host}{'mac'};
		$inputstring .=
		    "\n  <tr>\n    <td>" 
		  . $host
		  . "</td>\n    <td><a href=\"${wakecgi}$macAddress&psname=".$hosts->{$host}{'bootoptions'}."&host=".$host."&type=".$hosts->{$host}{'boottype'}."\">Start</a></td>\n";
		$inputstring = $inputstring . "\n  </tr>";
	    }
	}

	


	$inputstring = $inputstring . "\n</table>";
	if ($debug){
	  output("HTML text is $inputstring\n");
	}

	open(FH, ">/tmp/waky.html") || die "Could not open output file\n";
	print FH ($inputstring);
	close(FH);
	
    if($ini{html}{location} =~ m/^file:\/\/(.+)/)
    {
	 #changed to get true return values (does not work with qx) -> specific values not that important
         my $result = system("mv /tmp/waky.html  $1");
                  
         
         if(($result >> 8)!=0)
         {
            print("Error occured moving file to $1\n");              
         }
    }
    elsif($ini{html}{location} =~ m/^scp:\/\/(.+)@(.+):(.+)/)
    {
	#changed to get true return values (does not work with qx) -> specific values not that important
        my $result = system("scp /tmp/waky.html  $1\@$2:$3");
        
        if(($result>>8)!=0)
        {
            print("Error occured moving file to $1\n");              
             
        }
        else
        {
            qx(rm /tmp/waky.html);
        }
    }
    else
    {
        print("No valid output location specified. Check config.\n");
    }
}

#
#Function that updates the host information in the mediawiki front page
#
sub editMediaWiki
{
	binmode STDOUT, ':utf8';
	my $hosts        = shift;
	my %printoptions = ( 'memory' => 1, 'users' => 1, 'load' => 1 );
	system("kinit -k");
	my $login  = $ini{'mediawiki'}{'user'};
	my $pass   = $ini{'mediawiki'}{'pass'};
	my $domain = $ini{'mediawiki'}{'domain'};
	my $url    = $ini{'mediawiki'}{'url'};
		
	my $wakecgi = $ini{'mediawiki'}{'wakecgi'};
	my $pagename = $ini{'mediawiki'}{'pagename'};

	my $mw = MediaWiki::API->new();
	$mw->{config}->{api_url} = $url;

	# not needed cause of kerberos login
	#    $mw->login(
	#        {
	#            lgname     => $login,
	#            lgpassword => $pass,
	#            lgdomain   => $domain
	#        }
	#    ) || die $mw->{error}->{code} . ': ' . $mw->{error}->{details};

	my $page      = $mw->get_page( { title => $pagename } );
	unless ( $page->{missing} )
	{
		my $timestamp = $page->{timestamp};
		my $string    = $page->{'*'};
		my $inputstring =
qq(<!-- TABELLE -->\n{| border=1 cellspacing=0 class="wikitable"\n|- class="hintergrundfarbe5\n! Online computer );
		my @hostlist = keys( %{$hosts} );
		foreach ( keys %{ $hosts->{ $hostlist[0] } } )
		{
			if ( exists( $printoptions{$_} ) )
			{
				$inputstring .= "!! $_ ";
			}
		}
		foreach my $host ( keys %{$hosts} )
		{
			if ( $hosts->{$host}{'online'} )
			{
				$inputstring .= "\n|-\n| $host";
				foreach my $service ( keys %{ $hosts->{$host} } )
				{
					if ( exists( $printoptions{$service} ) )
					{

						#if($hosts->{$host}{$service})
						#{
						if ( $service eq 'memory' )
						{
							my $roundedmem = sprintf( "%.2f", $hosts->{$host}{$service} ) * 100;
							$inputstring .= "|| $roundedmem%";
						}
						else
						{
							$inputstring .= "|| $hosts->{$host}{$service}";
						}

						#}
						#else
						#{
						#        $inputstring .= "|| ";
						#}
					}
				}
			}
		}

		$inputstring .=
qq(\n|}\n\n\n{| border=1 cellspacing=0 class="wikitable"\n|- class="hintergrundfarbe5\n! Offline computer !! Boot );
		
		# to replace @macsdown
		foreach my $host (keys %$hosts){
		  if (!$hosts->{$host}{'online'} && $hosts->{$host}{'bootable'} == 1){
		    my $macAddress = $hosts->{$host}{'mac'};
		    $inputstring =
			    $inputstring
			  . "\n|-\n| "
			  . $host
			  . "|| [$wakecgi"
			  . $macAddress
			  . "Start] ";
		  }
		}
		
		$inputstring = $inputstring . "\n|}\n\n<!-- TABELLE -->";
		if ($debug){
		  output("The inputstring is $inputstring");
		}
		$string =~ s/<!-- TABELLE -->.*<!-- TABELLE -->/$inputstring/msg;
		$mw->edit(
			{
				action        => 'edit',
				title         => $pagename,
				basetimestamp => $timestamp,    # to avoid edit conflicts
				bot           => 1,
				text          => "$string"
			}
		) || die $mw->{error}->{code} . ': ' . $mw->{error}->{details};
	}
}

#Decide function
#
#We start the algorithm with one computer
# -Function decides which computers should be used for login -> rewrite of the bind config
# -determines computers which can be shut down
# -decides if we need new computers to boot
# Algorithm:
#    1a. Find the 4 computers with the lowest load
#    1b. calculate the average load
#    2. try to shut down the computer with the lowest load and load below a limit of 0.1
#    3. write the rest of the computers into the bind conf
#    4. if the average load > boot_limit => boot a computer
# parameters:
# $hosts:  pointer to %hosts

sub decideAndExecute
{
	my $hosts   = shift;
	my %hostdown = ();
	my $av_cf   = 0.0;     # average-cf
	foreach my $host ( keys( %{$hosts} ) )
	{
	  if ( $hosts->{$host}{'online'} eq 0 ){
	    if ( keys(%hostdown) == 0) 
            { 
                %hostdown = %{$hosts->{$host}}; 
                $hostdown{'name'} = $host; 
            }
            $hosts->{$host}{'update'} = 0;
            #delete( $hosts->{$host});
	  } else {
		  $av_cf += $hosts->{$host}{'cf'};
		  $hosts->{$host}{'update'} = 1;
	  }
	}
	my $num_comp = scalar( keys( %{$hosts} ) );
	#number of computers
	# Do we need to boot a new computer ?
	output("** Do we need to boot a new computer?\n");
    #average value
    if ($num_comp == 0) 
    {
        $av_cf = 0.0;
    } else {
    	$av_cf = $av_cf / $num_comp;                
    }
	output("Average cost functional: : $av_cf\n");
	output( "Wakeup limit cf: " . $WAKEUP_LIMIT . "\n" );
	if ( $av_cf > $WAKEUP_LIMIT )
	{
		output("Booting ". $hostdown{'name'} ."\n");
		if ( $hostdown{'boottype'} eq "power" ) 
        {
            switchOnPowerSupply($hostdown{'bootoptions'},$hostdown{'name'});
        } else {
		    qx(wakeonlan -i 134.76.83.255 $hostdown{'mac'});
		}
	}
	else
	{
		output("No need to boot something\n");
	}

	output("** Can we shut down a computer?\n");
	foreach my $host ( keys( %{$hosts} ) )
	{
		if ( $hosts->{$host}{'update'} == 1){
		if ( $hosts->{$host}{'cf'} > $WAKEUP_LIMIT + 0.4 )
		{
			#find computer with  cf > WAKEUP_LIMIT
			#
			$hosts->{$host}{'update'} = 0;
			#delete( $hosts->{$host} );
			output("$host has too high cf, skipping..\n");
		}
		elsif ($hosts->{$host}{'cf'} < $SHUTDOWN_LIMIT
			&& $hosts->{$host}{'users'} eq "0"
			&& $num_comp > 1
			&& $hosts->{$host}{'powerdownflag'} eq "1" )
		{
			#try to shut down all computers where no one is logged in and which have a small cf
			output(
"** Load small threshold of $SHUTDOWN_LIMIT, no users logged in (nagios), trying to shut down $host..\n"
			);
			if ( &powerdown($host) == 0 )
			{	
				my $domain = $hosts->{$host}{'domain'};
				if ($ini{'options'}{'x2go'} == 1) { setX2GOSerialNumber( $host, $domain, 0 ); }
				$hosts->{$host}{'update'} = 0;
				#delete( $hosts->{$host} );
			}
		}
		}
	}
	my @sortedlist = ();
	foreach my $host ( keys( %{$hosts} ) ){
	  if ( $hosts->{$host}{'update'} == 1){
	    push(@sortedlist,$host);
	  }
	}
	#@sortedlist = (sort{%$a->{'cf'} cmp %$b->{'cf'} } @sortedlist);
	# create a sorted array of our cf
	# my @sortedlist = ( sort { $hosts->{$a}{'cf'} cmp $hosts->{$b}{'cf'} } keys %{$hosts} );

	
    my @ips = ();
	# change bind-conf and restart the service
	if ( !$dryrun && $bindconf )
	{
		output("** Rewrite Bind-config\n");
		for ( my $count = 0 ; $count < $num_comp ; $count++ )
		{
			if ( ( $sortedlist[$count] ) )
			{
				output("Computer $count $sortedlist[$count] $hosts->{$sortedlist[$count]}{'cf'}\n");
				push(@ips,$hosts->{ $sortedlist[$count] }{'ip'});
			}
		}
	my @filename = glob '/etc/Kddnsupdatekey.*';
	if (scalar(@filename) == 0){
	    output("If you want the dynamic update of the nameserver, create a keypair using dnssec-keygen and update the bind files accordingly.");
	}
	else {
	     updateBind(\@ips);
	}
       	}
}



sub updateBind{
    my $ips = shift;
    my $domain = $ini{'rewrite'}{'domain'};
    my $dnsfile = $ini{'rewrite'}{'file'};
    qx(echo 'update delete $domain.$dnsfile A' >> update.bind);
    foreach my $ip ( @{$ips} ){
	  qx(echo 'update add $domain.$dnsfile 86400 A $ip\' >> update.bind);
    }
    qx(echo 'send'>> update.bind);
    my $result=system("nsupdate -d -k \"/etc/Kddnsupdatekey.+157+12177.private\" update.bind 2>1 > nsupdate.log");
    if (($result>>8)!=0){
	  output("An error occured while using nsupdate. Check nsupdate.log");
    }
    qx(rm update.bind);
}

# heuristic cost functional
sub costFunctional
{
	my $host  = shift;
	my $hosts = shift;
	if (   ( $hosts->{$host}{'load'} )
		&& ( $hosts->{$host}{'memory'} )
		&& ( $hosts->{$host}{'users'} > -1 ) )
	{
		$hosts->{$host}{'cf'} = $hosts->{$host}{'load'} + $hosts->{$host}{'memory'} + $hosts->{$host}{'users'} / 30;
	}
	else
	{
		$hosts->{$host}{'cf'} = 0;
	}
}

# Power down a computer
sub powerdown
{
	my $host  = shift;
	my $users = system("./check_remote_user $host $snmpPass $snmpUser | awk '{print \$4}'");
	chomp($users);
	if ( ($users >> 8) eq "0" )
	{
		my @busy = qx(ssh root\@$host 'if [ -e /var/lib/puppet/state/puppetdlock ]; then echo 1; else echo 0; fi');
		chomp(@busy);
		if ( defined( $busy[0] ) and ( $busy[0] eq '0' ) )
		{
			if ( !$dryrun )
			{
				qx(ssh root\@$host halt);
				output("Computer $host was shut down.\n");
				return 0;
			} else {
                output("dryrun: Computer $host was *not* shut down.\n");
            }
		}
	}
	else
	{
		output("Computer $host was *not* shut down.\n");
		return 1;
	}
	return 1;
}

sub checkNecessityOfSwitchOff{
	my $host = shift;
	my $result = system("./netio230a/switchPower.py -n $host -c yes -v");
	# system call has to be shifted 8 bits to the right to return proper return value
	$result = $result >>8;
	return $result;
}
sub switchOffPowerSupply
{
	my $psname = shift;
	my $host = shift;
	print qx(./netio230a/switchPower.py -d $psname -n $host -s off -v );
}

sub switchOnPowerSupply
{
	my $psname = shift;
	my $host = shift;
	print qx(./netio230a/switchPower.py -d $psname -n $host -s on -v);
}

sub readConfigFromMySQL
{
	my $hosts    = shift;
	my $groups   = shift;
	my $hostname = $ini{'config-mysql'}{'host'};
	my $database = $ini{'config-mysql'}{'database'};
	my $user     = $ini{'config-mysql'}{'user'};
	my $pass     = $ini{'config-mysql'}{'pass'};
	my $dsn      = "DBI:mysql:database=$database;host=$hostname";
	my $dbh      = DBI::->connect( $dsn, $user, $pass, { 'RaiseError' => 1, 'AutoCommit' => 1 } ) or die DBI::errstr;
	my $sth      = $dbh->prepare(
"SELECT objects.name,computers.mac,computers.ip,computers.bootable,computers.last_update,computers.updateflag,objects.id FROM computers JOIN objects WHERE computers.object=objects.id AND objects.type='computer'; "
	) or die "Couldn't prepare statement: " . $dbh->errstr;
	$sth->execute();
	my @data = ();

	while ( @data = $sth->fetchrow_array() )
	{
		$hosts->{ $data[0] }{'mac'}         = $data[1];
		$hosts->{ $data[0] }{'ip'}          = $data[2];
		$hosts->{ $data[0] }{'bootable'}    = $data[3];
		$hosts->{ $data[0] }{'last_update'} = $data[4];
		$hosts->{ $data[0] }{'updateflag'}  = $data[5];
		$hosts->{ $data[0] }{'id'}          = $data[6];
		#Default initialization of powerdownflag;
		$hosts->{$data[0]}{'powerdownflag'} =1;
		#get all groups of this host
		my $groupquery = $dbh->prepare("SELECT parent FROM relations WHERE child=$data[6];");
		$groupquery->execute();
		my @hostgroups = ();
		while ( @hostgroups = $groupquery->fetchrow_array() )
		{
			$hosts->{ $data[0] }{'groups'} = ();
			foreach my $group (@hostgroups)
			{
				if ($verbose) { output("Found $group for $data[0]\n"); }
				push( @{ $hosts->{ $data[0] }{'groups'} }, $group );

				#check if we allready know this group, add it to the groups array if not
				if ( !exists( $groups->{$group} ) )
				{
					$groups->{$group} = ();
				}
			}
		}

		#fetch all group attributes from sql now
		foreach my $group ( keys %{$groups} )
		{
			my $groupAttributeQuery = $dbh->prepare(
"SELECT wiki.url,wiki.user,wiki.password,wiki.domain,ndo.host,ndo.database,ndo.user,ndo.password,groups.wakeflag FROM `groups` join wiki join ndo WHERE wiki.id=groups.wiki and ndo.id=groups.ndo and object=$group;"
			);
			$groupAttributeQuery->execute();
			my @groupAttributes = ();
			while ( @groupAttributes = $groupAttributeQuery->fetchrow_array() )
			{
				$group->{'wiki'}{'url'}      = $groupAttributes[0];
				$group->{'wiki'}{'user'}     = $groupAttributes[1];
				$group->{'wiki'}{'password'} = $groupAttributes[2];
				$group->{'wiki'}{'domain'}   = $groupAttributes[3];
				$group->{'ndo'}{'host'}      = $groupAttributes[4];
				$group->{'ndo'}{'database'}  = $groupAttributes[5];
				$group->{'ndo'}{'user'}      = $groupAttributes[6];
				$group->{'ndo'}{'password'}  = $groupAttributes[7];
				$group->{'wakeflag'}         = $groupAttributes[8];
			}
		}
	}
	$dbh->disconnect();
}

sub LDAPmodifyUsingHash
{
	my ( $ldap, $dn, $whatToChange ) = @_;
	my $result = $ldap->modify( $dn, replace => {%$whatToChange} );
	return $result;
}

sub setX2GOSerialNumber
{
	my $hostname = shift;
	my $domain = shift;
	my $value    = shift;
	my $ldap     = Net::LDAP->new( $ini{'config-ldap'}{'host'} ) or die "LDAP-Verbindung fehlgeschlagen!\n";
	my $password = $ini{'config-ldap'}{'pass'};
	if ($debug) {output("LDAP-Password: $password\n")};
	my $mesg = $ldap->bind( $ini{'config-ldap'}{'user'}, password => $password ) or die "LDAP-Bind fehlgeschlagen!\n";
	if ( $mesg->{'resultCode'} != 0 )
	{
		die "LDAP ERROR: "
		  . $mesg->{'resultCode'} . ": "
		  . Net::LDAP::Util::ldap_error_text( $mesg->{'resultCode'} ) . "\n";
	}
	my %ReplaceHash = ( serialNumber => $value );
	my $base        = $ini{'config-ldap'}{'searchbase'};
	if($debug){output("LDAP-searchbase: $base\n")};
	my $dn          = "cn=$hostname.$domain," . $base;
	if ($debug){output("dn: $dn\n")};
	output ("Host: $hostname |  Value: $value | DN: $dn\n");
	my $result = LDAPmodifyUsingHash( $ldap, $dn, \%ReplaceHash );
	if ( $result->code )
	{
		LDAPerror( "Modifying", $result );
	}
}

sub LDAPerror
{
	my ( $from, $mesg ) = @_;
	output( "Return code: " . $mesg->code );
	output( "\tMessage: " . $mesg->error_name );
	output( " :" . $mesg->error_text );
	output( "MessageID: " . $mesg->mesg_id );
	output( "\tDN: " . $mesg->dn );
}

sub LDAPsearch
{

	my ( $ldap, $searchString, $attrs, $base ) = @_;

	#print "Base: $base\nFilter: $searchString\nAttrs: $attrs\n";
	my $result = $ldap->search(
		base   => "$base",
		scope  => "sub",
		filter => "$searchString",
		attrs  => $attrs
	) or die ("LDAP-search failed");
	return $result;
}

sub readConfigFromLDAP
{
	my $hosts  = shift;
	my $groups = shift;
	my $ldap   = Net::LDAP->new( $ini{'config-ldap'}{'host'} ) or die "LDAP-connection failed!\n";

	my $password = $ini{'config-ldap'}{'pass'};
	if($debug){output("Your LDAP-password is: $password\n");}
	my $mesg = $ldap->bind( $ini{'config-ldap'}{'user'}, password => $password );
	if ($verbose && $mesg->{'resultCode'} == 0){
		output("LDAP query successful, result code is " .  $mesg->{'resultCode'} . "\n");
	}
	
	elsif ( $mesg->{'resultCode'} != 0 )
	{
		die "LDAP ERROR: "
		  . $mesg->{'resultCode'} . ": "
		  . Net::LDAP::Util::ldap_error_text( $mesg->{'resultCode'} ) . "\n";
	}
	my $searchbase = $ini{'config-ldap'}{'searchbase'};
	if($debug){output("LDAP-searchbase is: $searchbase\n");}
	my $filter = $ini{'config-ldap'}{'filter'};
	if($debug){output("LDAP-filter is: $filter\n");}
	my @attrs = ( "objectClass", "ipHostNumber", "macAddress", "environment", "bootParameter" );

	# print "HOST: " . $ini{'config-ldap'}{'host'} . "\n";
	# print "USER: " . $ini{'config-ldap'}{'user'} . "\n";
	#  output("PASS: " . $ini{'config-ldap'}{'pass'} . "\n");
	#  print "SEARCHBASE: " . $searchbase . "\n";
	if($debug){output("LDAP-search starts now...\n");}
	my $result = LDAPsearch( $ldap, $filter, \@attrs, $searchbase );
	
	my $href = $result->as_struct;

	my @arrayOfDNs = keys %$href;    # use DN hashes
	if($debug){output("Array of DNs: @arrayOfDNs\n");}
	foreach my $DN (@arrayOfDNs)
	{	
		if($debug){output("Trying to find hostname and domain using a regex...\n");}
		$DN =~ m/cn=(.+?)\.(.+?),.+/;
		my $hostname = $1;
		if($debug){output("Hostname is: $hostname\t Domain is: $2\n");}
		my $valref   = $$href{$DN};
		#if ($verbose) { output($DN, "\n"); }
		# Default entry for powerdownflag generated here
		$hosts->{$hostname}{'powerdownflag'}=1;
		$hosts->{$hostname}{'domain'}=$2;
		if ( exists( $valref->{'objectclass'} ) )
		{
			$hosts->{$hostname}{'bootable'} = grep( /bootableDevice/, @{ $valref->{'objectclass'} } );
			if($debug){output("Host is bootable?: $hosts->{$hostname}{'bootable'}\n");}
		}

		if ( exists( $valref->{'bootparameter'} ) )
		{
			my $bootParameter = ( @{ $valref->{'bootparameter'} } )[0];
			$bootParameter =~ m/type=(wol|power):([a-zA-Z0-9-_]+)$/;
			$hosts->{$hostname}{'boottype'}    = $1;
			$hosts->{$hostname}{'bootoptions'} = $2;
			if($debug){output("Host's boottype?: $1\tHost's bootoptions: $2\n");}
		}
		# default setting for boottype is wakeonlan to avoid uninitialized value
		# default setting for bootoptions is set to default to avoid uninitialized value
		else {
			if($debug){output("Default setting for boottype and -option set, because no information could be found.\n");}
			$hosts->{$hostname}{'boottype'}    = "wakeonlan";
			$hosts->{$hostname}{'bootoptions'} = "default";
			if($debug){output("Host's boottype?: $hosts->{$hostname}{'boottype'}\tHost's bootoptions: $hosts->{$hostname}{'bootoptions'}\n");}
		}

		if ( exists( $valref->{'iphostnumber'} ) )
		{	
			my $ip = ( @{ $valref->{'iphostnumber'} } )[0];
			$hosts->{$hostname}{'ip'} = $ip;
			if($debug){output("Host's ip?: $ip\n");}
		}
		if ( exists( $valref->{'macaddress'} ) )
		{	
			my $mac=( @{ $valref->{'macaddress'} } )[0];
			$hosts->{$hostname}{'mac'} = $mac;
			if($debug){output("Host's mac adress?: $mac\n");}
		}
		if ( exists( $valref->{'environment'} ) )
		{
			if ( ( @{ $valref->{'environment'} } )[0] eq "disable" )
			{	
				if($debug){output("Host deleted because environment is disabled.\n");}
				delete( $hosts->{$hostname} );
			}
		}
	}
}

#read in the config file and write it to the hash $hosts (the parameter has to be a reference to a hash)
sub readConfig
{
	my $hosts  = shift;
	my $groups = shift;
	my $source = shift;
	output("Config source is $source\n");
	
	if ( $source eq "file" )
	{
		if($debug){output("Reading configuration from file...\n")};
		foreach my $section ( keys(%ini) )
		{	
			my $sectiontype = $ini{$section}{'type'};
			my $mac=$ini{$section}{'mac'};
			my $bootable=$ini{$section}{'bootable'};
			if($debug){output("Section in ini-file: $section.\tSection-type: $sectiontype\tMac-Adress: $mac\tBootable?: $bootable\n");}
			if ( ( $sectiontype eq 'computer' ) )
			{
				$hosts->{$section}{'mac'}      = $mac;
				$hosts->{$section}{'bootable'} = $bootable;
				if ($verbose) { output("$mac\n"); }
			}
		}
	}
	elsif ( $source eq "config-mysql" )
	{
		if($debug){output("Reading configuration from mySQL...\n")};
		&readConfigFromMySQL( $hosts, $groups );
	}
	elsif ( $source eq "config-ldap" )
	{	
		if($debug){output("Reading configuration from LDAP...\n")};
		&readConfigFromLDAP( $hosts, $groups );
	}
	else
	{
		output("No usable config backend found...exiting\n\n");
		exit(2);
	}
}

#execute a command and get the return value back
sub executeCommand
{
	my $command = join ' ', @_;
	( $_ = qx{$command 2>&1}, $? >> 8 );
}
