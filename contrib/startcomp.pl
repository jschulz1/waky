#!/usr/bin/perl
use strict;
use warnings;


sub printHTMLheader 
{
	print "Content-Type: text/html\n\n";
	print qq(<?xml version="1.0" encoding="utf-8"?>\n);
	print qq(<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"\n);
	print qq("http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">\n);
	print qq(<html xml:lang="de" lang="de">\n);
	print "<html>\n";
	print "<head>\n";
    print "<script type=\"text/javascript\">";
    print "window.location.href='$ENV{HTTP_REFERER}';";
    print "</script>";
	print "<title>Comp-Starter</title>\n";
	print "</head>\n";
	print "<body>\n";
}



sub main {
  
	&printHTMLheader;
	my $data="";
 	if($ENV{'REQUEST_METHOD'} eq 'GET')
	{
    		$data = $ENV{'QUERY_STRING'};
    	}
	else 
	{
		$data=$ARGV[0];
    		#read(STDIN, $data, $ENV{'CONTENT_LENGTH'} );
  	}
  	my @dataarray=split(/&/,$data);
  	my %datahash=();

	foreach my $formfield (@dataarray)
	{
		my ($key, $value) = split(/=/, $formfield);
		$value =~ tr/+/ /;
		$value =~ s/%([a-fA-F0-9].)/pack("C", hex($1))/eg;
		$value =~ s/<!--(.|\n)*-->//g;
		$datahash{$key}=$value;
	}
 	print "Starting $datahash{'host'}\n<br>"; 
	if ( $datahash{'type'} eq "power" ) 
	{
		print(qx(/opt/netio230a/switchPower.py -d $datahash{'psname'} -n $datahash{'host'} -s on -v ));
	} 
	else
	{
		print(qx(wakeonlan -i $datahash{'ip'} $datahash{'mac'}));
	}
 	#print(qx(wakeonlan $datahash{'mac'}));
     
  	print "</font>\n";
	print "</body>\n";
	print "</html>\n";
}

&main;

