#!/usr/bin/perl

use strict;
use warnings;

my @neededModules = ("Config::IniFiles","DBINet::LDAP","Net::LDAP::Entry","Net::LDAP::LDIF","Net::LDAP::Util","DateTime::Format::MySQL","Socket","MediaWiki::API","JSON","LWP::UserAgent","Proc::PID::File","Proc::Daemon",
"Log::Dispatch","Log::Dispatch::File","Date::Format","File::Spec","Getopt::Long","Digest::MD5");
eval{
	        require CPAN::Shell;
		require ExtUtils::Installed;
};

if($@){
	        print("For the automatic install of perl modules, you have to download the packages CPAN::Shell and ExtUtils::Installed from www.cpan.org\n");
}
else {
	use ExtUtils::Installed;
	my $inst = ExtUtils::Installed->new();
	my @modules = $inst->modules();
        use CPAN;
        use CPAN::Shell;
        foreach my $module (@neededModules){
		my $boolean =1;
		foreach my $installedModule (@modules){
			if ($module eq $installedModule){
				$boolean =0;
				last;
			}
		}
		if ($boolean==0){
			print("$module is already installed!\n");
		}
		else {
			print("$module is being installed from CPAN...\n");
                        CPAN::Shell->install($module);
		}
	}
}

		
	                
