# Waky

## Introduction

Waky.pl is a script designed to automatically monitor the workload of computers and, if desired, to shut down computers which are unused at the current time or to boot additional ones in case the others are working to capacity. Used for that is a heuristic cost functional, considering the users, memory and cpu usage and general load of the computers. Further functions are the automatic update of a webpage listing all computers and whether or not they are online and the dynamic update of DNS entries. Waky can either be run daemonized or console based. Necessary for the booting and powerdown process is either the enabling of wakeonlan on the target computers (for booting/shutting down with wakeonlan) or the Netio230a power supply (for the power induced boot/shut down process). Other programmable power supplies are not supported as of now. The required data of the computers can either be accessed through the configuration file, or via an ldap or mysql query.

## Configuration

Place the waky.conf file in /etc/ and set the desired values and information for a usage (See 5.). (If waky.conf should be placed in another directory, change waky.pl where the file location is listed at the beginning). Install the dependencies (see DEPENDENCIES). Most have to be installed manually, the perl modules can either be downloaded via CPAN or invoke the makefile.pl script, which will attempt to download all necessary modules. Waky was tested and developed on a Ubuntu 10.04.4 LTS system, so in order to make the script work universally, some script editing of waky.pl may be necessary.

## Usage

Waky is simply invoked by typing "perl waky.pl" in the working directory or "./waky.pl" in case the file was made executable before. No additional parameters are required.

## Detailed Description

Waky starts by reading out the configuration file waky.conf (is supposed to be put in /etc/waky.conf) to check, which processes are meant to be run, how to get access to the required data of the computers, and how the output is supposed to be. (For more information about waky.conf, see section 4. Waky.conf) After the gathering of the host data, all computers are pinged to check, which one is online and which is not. In case a computer is connected to a Netio230a power supply and is offline, the supply is switched off in case it was not before (Otherwise the automatic booting may not work). Afterwards, the host data (user number, memory usage, cpu usage, load etc.) is acquired from a backend, which is specified in the prementioned config file (can be either a nagios/mysql or a json/icinga query). The cost functional (cf) of each individual computer and their average is calculated and the web page is updated, if chosen to. Then, the decision is made whether or not a computer has to be booted or can be shut down. The mandatory prerequisites for shutting down are that the average cf of all online computers lies below a predefined value (see waky.conf) and that there is a computer, on which no users are logged in. A new computer is booted, when the average cf value lies above a predefined maximum. Finally, the nameserver is updated dynamically using nsupdate. In order to allow the update, you have to manually create a keypair using dnssec-keygen called Kddnsupatekey and have to put the .private key in /etc/. Also, the bind files have to be edited accordingly.

## Configuration File (waky.conf)

Instead of using command line parameters for the changing of options, every necessary value can be set in waky.conf (location is /etc/waky.conf). There are 8 sections, each of them containing information about a subprocess. What can be entered for each subprocess can be directly seen in waky.conf.template .


